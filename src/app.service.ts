import { ForbiddenException, Injectable } from '@nestjs/common';
import { MatrixUtilities } from './utilities/matrix';
import { BusinessValidator } from './validators/business-validator';

@Injectable()
export class AppService {
  private _validator: BusinessValidator = new BusinessValidator();
  private _matrix: MatrixUtilities = new MatrixUtilities();

  constructor(){
  }

  hasMutation(dna: string[]): Boolean {
    let sequence: number = 0;
    
    //Validation rules
    this._validator.validateMinLength(dna.length)
    this._validator.validateLongEquals(dna);
    
    //Find Horizontal Sequences
    dna.forEach(row => {
      sequence += this.findSequence(row);
    });

    //Find Vertical Sequences 
    this._matrix.transponseToVertical(dna).forEach(row => {
      sequence += this.findSequence(row);
    });

    //Find Oblique Sequences
    this._matrix.transponseToDiagonal(dna).forEach(row => {
      sequence += this.findSequence(row);
    });

    if(sequence <= 1)
      throw new ForbiddenException("Not found mutation")

    return true;
  }

  findSequence(dnaRow: string): number{
    let count: number = 0; 

    count += RegExp('AAAA').test(dnaRow) ? 1 : 0;
    count += RegExp('TTTT').test(dnaRow) ? 1 : 0;
    count += RegExp('CCCC').test(dnaRow) ? 1 : 0;
    count += RegExp('GGGG').test(dnaRow) ? 1 : 0;

    return count;
  }
  
}
