import { Injectable, NotAcceptableException } from "@nestjs/common";

export class BusinessValidator {

    constructor(){
    }

    // @Description: Validation DNA Size Array 
    validateLongEquals(dna: string[]){
        let rowLength: number = Number(dna[0].length);
    
        dna.forEach((row, index) => {
          if(rowLength == row.length){
            rowLength = row.length;
            this.validateOnlyAllowedCharacters(row.toUpperCase(), index);
          }else
            throw new NotAcceptableException(`The array at position No. ${ index + 1 }: ${ row } does not meet the proper length.`)      
        });
      }

    // @Description: Validation DNA Min Lengh Array 
    validateMinLength(dnaLength: number){
      if(dnaLength == 0) 
        throw new NotAcceptableException(`The array does not contain information`);
    }

    // @Description: Validation DNA Allowed Characters
    validateOnlyAllowedCharacters(dnaRow: string, index: number){
      const re = new RegExp('A|T|C|G');
      
      if(!re.test(dnaRow))
        throw new NotAcceptableException(`The array at position No. ${ index + 1 }: ${ dnaRow } does not comply with the nitrogenous base characters of DNA: A, T, C, G.`)      
    }
}