export class MatrixUtilities {

    constructor(){}

    transponseToVertical(dna: string[]): string[] {
        let newDna: string[] = [];
        let transpose = dna.map((_, colIndex) => dna.map(row => row[colIndex]));
    
        for (let i = 0; i < dna.length; i++)
          newDna.push(transpose[i].join().replace(/,/g, ''));
           
        return newDna;
      }
    
      transponseToDiagonal(dna: string[]): string[] {
        let newRows = [];
        let newDna: string[] = [];
        let dnaMatrix = this.convertToMatrix(dna);
    
        dnaMatrix.forEach((col, i) => newRows.push(this.getDiagonalCols(dnaMatrix, i)) );
        dnaMatrix.forEach((row, i)=>{ 
          if (i>0)
            newRows.push(this.getDiagonalRows(dnaMatrix,i)); 
        });
    
        for (let i = 0; i < newRows.length; i++)
          newDna.push(newRows[i].join().replace(/,/g, ''));
    
        return newDna;
      }
    
      convertToMatrix(dnaArray: string[]){
        let finalMtx = [];
     
        dnaArray.forEach((adn)=>{
            
           let tmpArray = [];
            
           for(let i=0; i <= adn.length-1; i++)
              tmpArray.push(adn[i]);
           
           finalMtx.push(tmpArray);
            
        });
       
        return finalMtx;
      }
    
      getDiagonalCols(matrix: any, numberCol: number){
        let line = [];
    
        for (let i=0; i <= matrix.length-numberCol-1; i++)
          line.push(matrix[i][i+numberCol]);
      
        return line;
      }
    
      getDiagonalRows(matrix: any, nRow: number){
        let line = [];
    
        for (let i=0; i <= matrix.length-nRow-1; i++)
            line.push(matrix[i+nRow][i]);
      
        return line;
      }
}