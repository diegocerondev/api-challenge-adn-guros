import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { PostDnaDto } from './entity/post-dna.dto';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  @HttpCode(200)
  postMutation(@Body() body: PostDnaDto) : Boolean {
    return this.appService.hasMutation(body.dna);
  }
}
