import { ApiProperty } from "@nestjs/swagger";
import { IsArray } from "class-validator";

export class PostDnaDto{

    @ApiProperty()
    @IsArray()
    readonly dna: string[];
}