import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // Test  Adn with mutation excpect 200
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
      })
      .expect(200);
  });

  // Test  Adn whitout mutation excpect 403
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        "dna":["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"]
      })
      .expect(403);
  });

  // Test Adn empty invalid 
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        "dna":[""]
      })
      .expect(406);
  });  

  // Test Adn content invalid 
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        "dna":["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCATGTGTCTG"]
      })
      .expect(406);
  });

  // Test Adn content validate characters not allowed 
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        "dna":["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "331222"]
      })
      .expect(406);
  });

});
